<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

    <div id="container" class="container">
        <main id="main" class="site-main">

            <?php

            // Start the Loop.
            while (have_posts()) :
                the_post();


                ?>
                <div class="asi_product_detail">

                    <div class="single-product-img">
                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="">
                    </div>
                    <div class="single-product-details">
                        
                            <h3>
                                <?php the_title(); ?>

                            </h3>
                            <span>
                                Price :
                            <?php
                            echo '$'.get_post_meta(get_the_ID(), 'price', true);
                            ?>
                            </span>
                            <span>
                                Rating :
                            <?php
                            echo get_post_meta(get_the_ID(), 'rating', true);
                            ?>
                            </span>
                            <p>
                            <?php
                            echo the_excerpt();
                            ?>
                            </p>
                        
                        <div class="add-cart-form">
                            <form method="post" action="">
                                <input type="number" name="quantity" min="1" value="1">
                                <input type="hidden" name="product_id" value="<?php echo get_the_ID(); ?>" >
                                <input type="submit" name="add_to_cart" value="Add to cart">
                            </form>
                        </div>
                        <a href="cart">
                            <button>Cart</button>
                        </a>
                    </div>
                </div>
                <?php

            endwhile; // End the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
