<form action="" method="POST">
    <div class="checkout-main">

        <div class="checkout-address">

            <table>
                <tr>
                    <td>
                        Name
                    </td>
                    <td>
                        <input type="text" name="firstname" id="name">
                    </td>
                </tr>
                <tr>
                    <td>
                        Address
                    </td>
                    <td>
                        <textarea name="address" id="address" cols="30" rows="10"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        Contact Number
                    </td>
                    <td>
                        <input type="number" name="number" id="number">
                    </td>
                </tr>
            </table>

        </div>
        <div class="checkout-bill">
            <div>
                <?php
                    $cart_total=0;
                    $cart_items=[];
                    foreach ($items as $id=>$product) {
                        $prod = asi_getProduct($id);
                        $cart_total = $cart_total + $product['quantity']*$prod['price'];
                    }
                    ?>
                <span>Total Amount :<?php echo $cart_total; ?> </span>
            </div>
            <div>
                <input type="hidden" value="<?php echo $cart_total; ?>" name="total">
                <input type="submit" value="Proceed to pay" name=proceed>
            </div>
        </div>

    </div>
</form>