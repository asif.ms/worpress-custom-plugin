<div class="cart-main">
    <?php 
        $cart_total=0;
    ?>
    <div class="cart-items">
        <table>
            <thead>

                <th>
                    Product Name
                </th>
                <th>
                    Price
                </th>
                <th>
                    Quantity
                </th>
                <th>
                    Total
                </th>
            </thead>

            <tbody>
                <?php

                    foreach ($products as $id=>$product) {
                        $prod = asi_getProduct($id);
                        echo '<tr></tr><td>'.$prod['title'].'</td>';
                        echo '<td>'.$prod['price'].'</td>';
                        echo '<td>'.$product['quantity'].'</td>';
                        echo '<td>'.$product['quantity']*$prod['price'].'</td></tr>';
                        $cart_total = $cart_total + $product['quantity']*$prod['price'];
                    }
                    ?>
                <tr></tr>
                <tr>
                    <td>
                        Total Amount
                    </td>
                    <td></td>
                    <td></td>
                    <td>
                        <?php echo $cart_total; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="cart-summary">
        <form action="<?php bloginfo('url'); ?>/checkout/" method="POST">
            <input type="hidden" name="total" value="<?php echo $cart_total ?>">
            <input type="submit" value="Place Order" name="checkout">
        </form>
    </div>
</div>