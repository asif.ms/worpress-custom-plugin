<div class="container">
<?php
$btpgid=get_queried_object_id();
$btmetanm=get_post_meta( $btpgid, 'WP_Catid','true' );
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1, 
        'paged' => $paged,
    );
    $arr_posts = new WP_Query( $args );
                 
    if ( $arr_posts->have_posts() ) :
        echo '<div class="product-list">';
        while ( $arr_posts->have_posts() ) :
            $arr_posts->the_post();

            ?><div class="acd_product" style="border:1px solid black; margin-bottom:10px; text-align:center">
            <?php the_post_thumbnail(); ?>
            <h3>
                <a href="<?php echo get_the_permalink() ?> ">
                    <?php the_title(); ?>
                </a>
            </h3>
            <div>
                <?php
                echo "Price:".get_post_meta(get_the_ID(), 'price', true);
                echo '<br />';
                echo "Rating:".get_post_meta(get_the_ID(), 'rating', true).'.0 ';
                ?>
            </div>
        </div>
            <?php
        endwhile;
    echo'</div>';
    wp_reset_postdata();
    endif;
 
?>
</div>