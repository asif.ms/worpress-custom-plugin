import React from "react";
import style from './Orders.scss'
import FetchWP from '../utils/fetchWP';

class Orders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            orders: [],
            totalPages:0,
            
        }
        this.fetchWP = new FetchWP({
            restURL: window.acpbp_object.root,
            restNonce: window.acpbp_object.api_nonce,

        });
    }

    componentDidMount() {
        this.fetchData();

    }

    handlePage=(page)=>{
        this.fetchData({page});
    }

    fetchData(filter) {
        this.setState({
            loader: true,
        });
        const query=new URLSearchParams(filter).toString();

        this.fetchWP.get(`orders?${query}`)
            .then(
                ({data, totalPages}) => {
                    this.setState({
                        loader: false,
                        orders: data,
                        totalPages,
                    });
                });

    }


    render() {
        const { totalPages,orders }=this.state;
        return (<div className={style.container}>
            Orders Lists
            <table>
                <tr>
                    <th>Customer Name</th>
                    <th>Total Amount</th>
                    <th>Address</th>
                    <th>Contact Number</th>
                    <th>Order Status</th>
                </tr>
                {orders?.map(e => {
                    return (
                        <tr>
                            <td>{e.customer_name}</td>
                            <td>{e.order_amount}</td>
                            <td>{e.customer_address}</td>
                            <td>{e.contact_number}</td>
                            <td>{e.contact_status}</td>
                        </tr>
                    )
                })}
            </table>

            <div className={style.page_btn}>
                {[...Array(totalPages)]?.map((val,index)=><button onClick={()=>this.handlePage(index+1)} type="button">{index+1}</button>)}
            </div>
        </div>)
    }
}


export default Orders;
