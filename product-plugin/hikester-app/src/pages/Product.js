import React from "react";
import style from './Product.scss'
import FetchWP from '../utils/fetchWP';

class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            products: [],
            totalPages: 0,
        }
        this.fetchWP = new FetchWP({
            restURL: window.acpbp_object.root,
            restNonce: window.acpbp_object.api_nonce,

        });
    }

    componentDidMount() {
        this.fetchData();
    }

    handlePage = (page) => {
        this.fetchData({page});
    }

    fetchData(filter) {
        this.setState({
            loader: true,
        });
        const query = new URLSearchParams(filter).toString();

        this.fetchWP.get(`products?${query}`)
            .then(
                ({ data, totalPages }) => {
                    this.setState({
                        loader: false,
                        products: data,
                        totalPages,
                    });
                });

    }


    render() {
        const { totalPages, products } = this.state;
        return (<div className={style.container}>
            <h3>Products Lists</h3>
            <div className={style.productList}>
                {products?.map(e => {
                    return (
                        <div className={style.product}>
                            <div>
                                <img src={e.product_image}></img>
                            </div>
                            <div>
                                {e.product_name}
                            </div>
                            <div>
                                Price: {e.product_price}
                            </div>
                        </div>
                    )
                })}
            </div>
            <div className={style.page_btn}>
                {[...Array(totalPages)]?.map((val,index)=><button onClick={()=>this.handlePage(index+1)} type="button">{index+1}</button>)}
            </div>
        </div>
        )
    }
}


export default Product;
