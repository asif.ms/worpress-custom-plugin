<?php

/**
 * Plugin Name: Hikester
 * Version: 1.0.0
 * Description: Plugin  short description
 * Author: Asif
 * Author URI: http://asif.com
 * Requires at least: 4.4.0
 * Tested up to: 5.5.3
 * WC requires at least: 3.0.0
 * WC tested up to: 4.7.0
 */

define('ACPBP_TOKEN', 'acpbp');
define('ACPBP_VERSION', '1.0.0');
define('ACPBP_FILE', __FILE__);
define('ACPBP_PLUGIN_NAME', 'Acowebs Plugin Boiler Plate');

// Helpers.
require_once realpath(plugin_dir_path(__FILE__)) . DIRECTORY_SEPARATOR . 'includes/helpers.php';

// Init.
add_action('plugins_loaded', 'acpbp_init');
if (!function_exists('acpbp_init')) {
    /**
     * Load plugin text domain
     *
     * @return  void
     */
    function acpbp_init()
    {
        $plugin_rel_path = basename(dirname(__FILE__)) . '/languages'; /* Relative to WP_PLUGIN_DIR */
        load_plugin_textdomain('acowebs-plugin-boiler-plate-text-domain', false, $plugin_rel_path);
    }
}

// Loading Classes.
if (!function_exists('ACPBP_autoloader')) {

    function ACPBP_autoloader($class_name)
    {
        if (0 === strpos($class_name, 'ACPBP')) {
            $classes_dir = realpath(plugin_dir_path(__FILE__)) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;
            $class_file = 'class-' . str_replace('_', '-', strtolower($class_name)) . '.php';
            require_once $classes_dir . $class_file;
        }
    }
}
spl_autoload_register('ACPBP_autoloader');

// Backend UI.
if (!function_exists('ACPBP_Backend')) {
    function ACPBP_Backend()
    {
        return ACPBP_Backend::instance(__FILE__);
    }
}
if (!function_exists('ACPBP_Public')) {
    function ACPBP_Public()
    {
        return ACPBP_Public::instance(__FILE__);
    }
}
// Front end.
ACPBP_Public();
if (is_admin()) {
    ACPBP_Backend();
}
new ACPBP_Api();
