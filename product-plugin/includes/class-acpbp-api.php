<?php

if (!defined('ABSPATH')) {
    exit;
}

class ACPBP_Api
{


    /**
     * @var    object
     * @access  private
     * @since    1.0.0
     */
    private static $instance = null;

    /**
     * The version number.
     *
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public $version;
    /**
     * The token.
     *
     * @var     string
     * @access  public
     * @since   1.0.0
     */
    public  $token;

    public function __construct()
    {
        $this->token = ACPBP_TOKEN;

        add_action(
            'rest_api_init',
            function () {
                register_rest_route(
                    $this->token . '/v1',
                    '/config/',
                    array(
                        'methods' => 'GET',
                        'callback' => array($this, 'getConfig'),
                        'permission_callback' => array($this, 'getPermission'),
                    )
                );

                register_rest_route(
                    $this->token . '/v1',
                    '/products/',
                    array(
                        'methods' => 'GET',
                        'callback' => array($this, 'getProducts'),
                        'permission_callback' => array($this, 'getPermission'),
                    )
                );

                register_rest_route(
                    $this->token . '/v1',
                    '/orders/',
                    array(
                        'methods' => 'GET',
                        'callback' => array($this, 'getOrders'),
                        'permission_callback' => array($this, 'getPermission'),
                    )
                );
            }
        );

    }

    public function getConfig()
    {
        $config = ['general' =>
            ['title' => 'Acowebs Boiler Plate']
        ];

        return new WP_REST_Response($config, 200);
    }

    public function getProducts($data)
    {
        $page = ($data['page']) ? $data['page'] : 1;
        $page_count = get_posts(array(
            'posts_per_page' => -1,
            'fields' => 'ids', 
            'post_type' => 'product'
        ));
        $page_count = count($page_count);
        $totalPages = $page_count/3;
        if(is_float($totalPages))
            $totalPages=intval($totalPages)+1;
        $all_products = get_posts ( array ( 
            'fields' => 'ids', 
            'posts_per_page' => '3', 
            'paged' => $page,
            'post_type' => 'product',
         ) );
        $result = array();
        foreach ($all_products as $product) {
            $result[] = array(
                'product_id' => $product,
                'product_name' => get_the_title($product) ? get_the_title($product) : 'No Label',
                'product_date' => get_the_date('d M Y', $product),
                'product_price' => get_post_meta($product,'price',true),
                'product_rating' => get_post_meta($product,'rating',true),
                'product_image' => get_the_post_thumbnail_url( $product),
                'get_link' => get_the_permalink($product),
                'pages' => $totalPages,
            );
        }
        $obj = (object) [
            'totalPages' => $totalPages,
            'data' => $result
        ];

        return new WP_REST_Response($obj, 200);
    }

    public function getOrders($data)
    {
        $page = ($data['page']) ? $data['page'] : 1;
        $page_count = get_posts(array(
            'posts_per_page' => -1,
            'fields' => 'ids', 
            'post_type' => 'orders'
        ));
        $page_count = count($page_count);
        $totalPages = $page_count/2;
        if(is_float($totalPages))
            $totalPages=intval($totalPages)+1;
        $all_orders = get_posts ( array ( 
            'fields' => 'ids', 
            'posts_per_page' => '2', 
            'paged' => $page,
            'post_type' => 'orders',
            'orderby'=> 'meta_value_num',
            'order'=> 'DESC',
        ));
        $result = array();
        foreach ($all_orders as $order) {
            $result[] = array(
                'order_id' => $order,
                'customer_name' =>get_post_meta($order,'firstname',true),
                'order_date' => get_the_date('d M Y', $order),
                'order_amount' => get_post_meta($order,'order_total',true),
                'customer_address' => get_post_meta($order,'address',true),
                'contact_number' => get_post_meta($order,'contact_number',true),
                'contact_status' => get_post_meta($order,'status',true),
                'get_link' => get_the_permalink($order),
                'pages' => $totalPages,
            );
        }
        $obj = (object) [
            'totalPages' => $totalPages,
            'data' => $result
        ];

        return new WP_REST_Response($obj, 200);
    }

    /**
     *
     * Ensures only one instance of APIFW is loaded or can be loaded.
     *
     * @param string $file Plugin root path.
     * @return Main APIFW instance
     * @see WordPress_Plugin_Template()
     * @since 1.0.0
     * @static
     */
    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Permission Callback
     **/
    public function getPermission()
    {
        if (current_user_can('administrator') || current_user_can('manage_woocommerce')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Cloning is forbidden.
     *
     * @since 1.0.0
     */
    public function __clone()
    {
        _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?'), $this->_version);
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 1.0.0
     */
    public function __wakeup()
    {
        _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?'), $this->_version);
    }
}
