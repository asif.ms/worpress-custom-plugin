<?php


function asi_getProduct($product_id)
{

    $post = get_post($product_id);
    return [
        'title' => $post->post_name,
        'price' => get_post_meta($product_id,'price',true),

    ];
}