<?php


class ASI_Cart
{

    public $products = [];

    public function __construct()
    {

        if( !session_id() )
        {
            session_start();
        }

        $this->products = isset($_SESSION['asi_cart'])?$_SESSION['asi_cart']:[];
    }

    function addToCart($product_id, $quantity = 1)
    {
        $product = asi_getProduct($product_id);

        if (isset($this->products[$product_id])) {
            $this->products[$product_id]['quantity'] = $quantity + $this->products[$product_id]['quantity'];
            $this->products[$product_id]['price'] = $this->products[$product_id]['price'] + $product['price'] * $quantity;
        } else {
            $this->products[$product_id] = ['quantity' => $quantity, 'price' => $product['price'] * $quantity];
        }

        $_SESSION['asi_cart'] = $this->products;
    }

    function getCart()
    {
        return $this->products;
    }

}