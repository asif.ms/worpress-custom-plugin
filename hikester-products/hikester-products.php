<?php

/*
Plugin Name: Hikester Product Plugin
Plugin URI: http://asif.com
Description: My first plugin
Author: asif
Version: 1.0.0
Author URI:
*/

class ASI_hikesterProduct
{
    public $assets_url;

    public function __construct()
    {
        add_action('init', array($this, 'custom_post_type'));
        
        add_action('init', array($this, 'custom_post_type_order'));

        add_action('add_meta_boxes', array($this, 'metaBoxes'));

        add_action('save_post', array($this, 'metaSave'));

        add_action('save_post', array($this, 'metaSaveOrder'));

        add_filter('template_include', array($this, 'template_include'));

        add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));

        $this->assets_url = esc_url(trailingslashit(plugins_url('/assets/', __FILE__)));
    
        $cart = new ASI_Cart();

        if (isset($_POST['add_to_cart']) && isset($_POST['product_id'])) {

            $cart->addToCart($_POST['product_id'], $_POST['quantity']);
        }

        add_shortcode('asi_cart',array($this,'cartView'));
    
        add_shortcode('asi_checkout',array($this,'checkOutView'));

        add_shortcode('asi_orders',array($this,'orderView'));
    
        add_action('init', array($this, 'addOrder'));

        add_action('admin_menu', array($this, 'adminPage'));
    }

    function activate()
    {
        flush_rewrite_rules();
    }

    // Create Custom Post type

    function custom_post_type()
    {
        $labels = array(
            'name' => _x('Products', 'Post type general name', 'recipe'),
            'singular_name' => _x('Product', 'Post type singular name', 'recipe'),
            'menu_name' => _x('Products', 'Admin Menu text', 'recipe'),
            'name_admin_bar' => _x('Product', 'Add New on Toolbar', 'recipe'),
            'add_new' => __('Add New', 'recipe'),
            'add_new_item' => __('Add New Product', 'recipe'),
            'new_item' => __('New Product', 'recipe'),
            'edit_item' => __('Edit Product', 'recipe'),
            'view_item' => __('View Product', 'recipe'),
            'all_items' => __('All Products', 'recipe'),
            'search_items' => __('Search Products', 'recipe'),
            'parent_item_colon' => __('Parent Products:', 'recipe'),
            'not_found' => __('No Products found.', 'recipe'),
            'not_found_in_trash' => __('No Products found in Trash.', 'recipe'),
            'featured_image' => _x('Recipe Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'recipe'),
            'set_featured_image' => _x('Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'recipe'),
            'remove_featured_image' => _x('Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'recipe'),
            'use_featured_image' => _x('Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'recipe'),
            'archives' => _x('Recipe archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'recipe'),
            'insert_into_item' => _x('Insert into Product', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'recipe'),
            'uploaded_to_this_item' => _x('Uploaded to this recipe', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'recipe'),
            'filter_items_list' => _x('Filter recipes list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'recipe'),
            'items_list_navigation' => _x('Recipes list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'recipe'),
            'items_list' => _x('Recipes list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'recipe'),
        );
        $args = array(
            'labels' => $labels,
            'description' => 'Recipe custom post type.',
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'product'),
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 2,
            'supports' => array('title', 'excerpt', 'author', 'thumbnail'),
            'taxonomies' => array('product_cat'),
            'show_in_rest' => true,
            'menu_icon'   => 'dashicons-products'
        );

        // Add Custom taxonomy
        $labels_tax = array(
            'name' => _x('Category', 'taxonomy general name', 'textdomain'),
            'singular_name' => _x('Category', 'taxonomy singular name', 'textdomain'),
            'search_items' => __('Search Category', 'textdomain'),
            'popular_items' => __('Popular Category', 'textdomain'),
            'all_items' => __('All Category', 'textdomain'),
            'parent_item' => null,
            'parent_item_colon' => null,
            'edit_item' => __('Edit Category', 'textdomain'),
            'update_item' => __('Update Category', 'textdomain'),
            'add_new_item' => __('Add New Category', 'textdomain'),
            'new_item_name' => __('New Category Name', 'textdomain'),
            'separate_items_with_commas' => __('Separate writers with commas', 'textdomain'),
            'add_or_remove_items' => __('Add or remove Category', 'textdomain'),
            'choose_from_most_used' => __('Choose from the most used Category', 'textdomain'),
            'not_found' => __('No Category found.', 'textdomain'),
            'menu_name' => __('Category', 'textdomain'),
        );
        $args_tx = array(
            'hierarchical' => true,
            'labels' => $labels_tax,
            'show_ui' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'rewrite' => array('slug' => 'product_cat'),
        );

        register_post_type('product', $args);

        register_taxonomy('product_cat', 'product', $args_tx);
    }

    // Create Custom Post type Order

   function custom_post_type_order()
   {
       $labels = array(
           'name' => _x('Orders', 'Post type general name', 'order'),
           'singular_name' => _x('Order', 'Post type singular name', 'order'),
           'menu_name' => _x('Orders', 'Admin Menu text', 'order'),
           'name_admin_bar' => _x('Order', 'Add New on Toolbar', 'order'),
           'add_new' => __('Add New', 'order'),
           'add_new_item' => __('Add New Order', 'order'),
           'new_item' => __('New Order', 'order'),
           'edit_item' => __('Edit Order', 'order'),
           'view_item' => __('View Order', 'order'),
           'all_items' => __('All Orders', 'order'),
           'search_items' => __('Search Products', 'order'),
           'parent_item_colon' => __('Parent Orders:', 'order'),
           'not_found' => __('No Orders found.', 'order'),
           'not_found_in_trash' => __('No Orders found in Trash.', 'recipe'),
           'featured_image' => _x('Recipe Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'recipe'),
           'set_featured_image' => _x('Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'recipe'),
           'remove_featured_image' => _x('Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'recipe'),
           'use_featured_image' => _x('Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'recipe'),
           'archives' => _x('Recipe archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'recipe'),
           'insert_into_item' => _x('Insert into Order', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'recipe'),
           'uploaded_to_this_item' => _x('Uploaded to this recipe', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'recipe'),
           'filter_items_list' => _x('Filter orders list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'recipe'),
           'items_list_navigation' => _x('Orders list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'recipe'),
           'items_list' => _x('Orders list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'recipe'),
       );
       $args = array(
           'labels' => $labels,
           'description' => 'Order custom post type.',
           'public' => true,
           'publicly_queryable' => true,
           'show_ui' => true,
           'show_in_menu' => true,
           'query_var' => true,
           'rewrite' => array('slug' => 'order'),
           'capability_type' => 'post',
           'has_archive' => true,
           'hierarchical' => false,
           'menu_position' => 2,
           'supports' => array('title', 'excerpt', 'author', 'thumbnail'),
           'taxonomies' => array('order_cat'),
           'show_in_rest' => true,
           'menu_icon'   => 'dashicons-products'
       );

   register_post_type('order', $args);

   }

    function metaBoxes()
    {
        add_meta_box('asi_pricing', __('Product Conf', 'textdomain'), array($this, 'metBoxView'), 'product');
        add_meta_box('asi_order', __('Oder Conf', 'textdomain'), array($this, 'metBoxOrder'), 'order');

    }

    // Meta box view Product

    function metBoxView($post)
    {

        $price = get_post_meta($post->ID, 'price', true);

        $rating = get_post_meta($post->ID, 'rating', true);

        ?>
        <div>
            <label>
                <p>Product Price</p>
                <input type="text" name="price" value="<?php echo $price; ?>">
            </label>
            <label>
                <p>Rating</p>
                <input type="number" name="rating" value="<?php echo $rating; ?>">
            </label>
        </div>

        <?php

    }

    // Meta Box view Order

    function metBoxOrder($post)
    {

        $firstname = get_post_meta($post->ID, 'firstname', true);

        $address = get_post_meta($post->ID, 'address', true);

        $phone = get_post_meta($post->ID, 'contact_number', true);

        $total = get_post_meta($post->ID, 'order_total', true);


        ?>
        <div>
            <label>
                <p>Customer Name</p>
                <input type="text" name="firstname" value="<?php echo $firstname; ?>">
            </label>
            <label>
                <p>Address</p>
                <textarea name="address" id="address" cols="30" rows="10" placeholder="<?php echo $address; ?>"></textarea>
            </label>
            <label>
                <p>Contact Number</p>
                <input type="number" name="contact_number" value="<?php echo $phone; ?>">
            </label>
            <label>
                <p>Order Total</p>
                <input type="number" name="order_total" value="<?php echo $total; ?>">
            </label>
        </div>

        <?php

    }
    
    // Meta Box Data save

    function metaSave($post_id)
    {

        if (isset($_POST['price'])) {
            update_post_meta($post_id, 'price', $_POST['price']);
            update_post_meta($post_id, 'rating', $_POST['rating']);
        }


    }

    // Oreder Meta Box Data save

    function metaSaveOrder($post_id)
    {

        if (isset($_POST['total'])) {
            update_post_meta($post_id, 'name', $_POST['firstname']);
            update_post_meta($post_id, 'address', $_POST['address']);
            update_post_meta($post_id, 'number', $_POST['number']);
            update_post_meta($post_id, 'total', $_POST['total']);
        }


    }

    // Cart View Page

    function cartView()
    {

        $cart = new ASI_Cart();
        $products = $cart->getCart();

        ob_start();

        include( plugin_dir_path(__FILE__) .'templates/cart.php' );

        return  ob_get_clean();


    }

    // Checkout view Page

    function checkOutView()
    {
        $cart = new ASI_Cart();
        $items = $cart->getCart();
        ob_start();

        include( plugin_dir_path(__FILE__) .'templates/checkout.php' );

        return  ob_get_clean();
    }

    // Order view Page

    function orderView()
    {
        ob_start();

        include( plugin_dir_path(__FILE__) .'templates/order.php' );

        return  ob_get_clean();
    }

    // Order Add Function

    function addOrder(){
        $cart = new ASI_Cart();

        if (isset($_POST['proceed']) && isset($_POST['total'])) {
            $my_order = array(
                'post_type' => 'order',
                'post_title' => $_POST['firstname'],
                'post_status' => 'publish',
                'meta_input' => array(
                    'firstname' => $_POST['firstname'],
                    'address' => $_POST['address'],
                    'contact_number' => $_POST['number'],
                    'order_total' =>  $_POST['total']
                )
            );

            wp_insert_post($my_order);

            echo "Order Placed Successfully";
            // $order->addToOrder($my_order);
        }
    }

    //Admin page settings

    function adminPage()
    {
        add_menu_page(
            __('Custom Menu Title', 'textdomain'),
            'Hikester menu',
            'manage_options',
            'hikester_settings',
            array($this, 'MenuPage'),
            null,
            6
        );


        if (isset($_POST['my_settings'])) {
            // add_option('mp_shipping',$_POST['mp_shipping']);
            if($_POST['gender']=="male")
            {
                update_option('gender-value',"you have clicked male details");
            }
            else{
                update_option('gender-value',"you have clicked female details");
            }
        }

        if (isset($_POST['get_ship'])) {
            update_option('gender-value',"you have clicked female details");
        }

    }

    function MenuPage()
    {
        include(plugin_dir_path(__FILE__) . 'templates/settings.php');
    }

    // Include Template

    function template_include($template)
    {

        if (is_single() && get_post_type() == 'product') {
            return plugin_dir_path(__FILE__) . 'templates/single-product.php';
        }

        if (is_tax('product_cat')) {
            return plugin_dir_path(__FILE__) . 'templates/archive-product.php';
        }

        return $template;
    }

    // enqueue Scripts

    function wp_enqueue_scripts()
    {
        wp_enqueue_style('product-style', $this->assets_url . 'style.scss');
        wp_enqueue_script('product-js', $this->assets_url . 'main.js');

    }
}

include(plugin_dir_path(__FILE__) . '/templates/registration.php');
include(plugin_dir_path(__FILE__) . '/templates/login.php');
include(plugin_dir_path(__FILE__) . '/cart.php');
include(plugin_dir_path(__FILE__) . '/functions.php');

$myClass = new ASI_hikesterProduct();

register_activation_hook(__FILE__, array($myClass, 'activate'));