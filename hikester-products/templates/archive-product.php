<?php
/**
 * The template for displaying all posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<div id="container" class="container">
    <main id="main" class="site-main">
    <div class="product-section">

    <?php

    // The Loop
    if (have_posts()) {
        echo '<div class="acd_product_list">';
        while (have_posts()) {
            the_post();

            ?>
            <div class="product-card">
                <div class="product-img" style="width:200px;">
                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="">
                </div>
                <div class="product-card-det">
                    <h3>
                        <a href="<?php echo get_the_permalink() ?> ">
                            <?php the_title(); ?>
                        </a>
                    </h3>
                    <span>
                        Price :
                        <?php
                        echo '$'.get_post_meta(get_the_ID(), 'price', true);
                        ?>
                    </span>
                    <span>
                        Rating :
                        <?php
                        echo get_post_meta(get_the_ID(), 'rating', true);
                        ?>
                    </span>
                </div>

            </div>
            <?php
        }
    }

    ?>
     </div><!-- #product-section-->
    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
